var json2csv = require('json2csv').parse;
var fs = require('fs');
var path = require('path')
var filepath = './downloadedfiles/'
var sampleJSON  = {
    'id_mesh': 'No data for the specified Data range',
    'value':'',
    'datetime':'', 
    'id_secret':'',
    'id_wasp':'',
    'id':'',
    'sensor':''
}

var convert2CSV = function(data, filename, callback){
    const jsonData = JSON.parse(JSON.stringify(data));
    const items = data.Count != 0 ? jsonData.Items : sampleJSON;
    var fields = Object.keys(sampleJSON);
    var csv = json2csv(items ,fields);
    file = filepath+filename
    fs.writeFile(file, csv, function(err) {
        if (err) callback(err);
        callback(null, path.resolve(file))  
    });
}

module.exports = {convert2CSV}