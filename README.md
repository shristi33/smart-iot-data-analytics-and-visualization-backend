# IoT Dashboard (Backend Server)

Backend web-service for the IoT Dashboard with REST API Endpoints.

### Prerequisites

1. [nodejs] (https://nodejs.org/en/)
2. [npm - package manager for node] (https://www.npmjs.com/)
3. A web browser (cough don't use Internet Explorer cough cough)
4. [pm2 - container for nodejs webservice] (http://pm2.keymetrics.io/)

### Installing

- clone the repo
- `npm install` to install all the dependencies
- `npm install -g nodemon` to install nodemon to run the server
- `nodemon` to start server
- Open web browser and navigate to `localhost:3000/api/sensors/all` to view the data. 

## Deployment
For deployment in a server, it's better to run the web-service in a separate process manager.

- `npm install pm2 -g` to install pm2
- `pm2 start index.js` to start the server

