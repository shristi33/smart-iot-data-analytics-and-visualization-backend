var AWS = require('aws-sdk');
const config = require('./config/config');
const utils = require('./utils');
var express = require('express'),
    app = express(),
    port = 3000;

app.use(express.static('public'));
app.listen(port);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/api/sensors/all', (req, res, next) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: config.aws_table_name
  };

  docClient.scan(params, function(err, data) {
    if (err) {
      res.send({
        success: false,
        message: 'Error: Server error'
      });
    } else {
      res.send({
        success: true,
        message: 'Loaded data',
        data
      });
    }
  });
}); 

app.get('/api/sensors', (req, res, next) => {
  var sensor = req.query.sensor;
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  var dateFrom = new Date(req.query.dateFrom);
  var dateTo = new Date(req.query.dateTo);
  var startDate = dateFrom.toISOString();
  var endDate = dateTo.toISOString();
  var id_wasp = req.query.node;
  var params = {
    TableName : config.aws_table_name,
    KeyConditionExpression: "#sensor = :sensor",
    FilterExpression: "#sensor = :sensor AND #startDate BETWEEN :datetime1 AND :datetime2 AND #id_wasp = :id_wasp",
    ExpressionAttributeNames:{
        "#sensor": "sensor",
        "#id_wasp": "id_wasp",
        "#startDate": "datetime"
    },
    ExpressionAttributeValues: {
        ":sensor": sensor,
        ":id_wasp": id_wasp,
        ":datetime1": startDate,
        ":datetime2": endDate

    }
  };
  var outputData = [];
  docClient.scan(params, onScan);
  function onScan(err, data) {
    if (err) {
      console.log(err);
      res.send({
        success: false,
        message: 'Error: Server error'
      });
    } else {
        outputData = outputData.concat(data.Items);
        if (typeof data.LastEvaluatedKey != "undefined") {
            console.log("Scanning for more...");
            params.ExclusiveStartKey = data.LastEvaluatedKey;
            docClient.scan(params, onScan);
        }else{
          res.send({
            success: true,
            message: 'Loaded data',
            data: {
              Items: outputData
            }
          });
        }
    }
  }
}); 

app.get('/api/sensors/download', (req,res,next) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  var id_wasp = req.query.node;
  var dateFrom = new Date(req.query.dateFrom);
  var dateTo = new Date(req.query.dateTo);
  var startDate = dateFrom.toISOString();
  var endDate = dateTo.toISOString();
  var params = {
    TableName : config.aws_table_name,
    KeyConditionExpression: "#id_wasp = :id_wasp",
    FilterExpression: "#startDate BETWEEN :datetime1 AND :datetime2",
    ExpressionAttributeNames:{
        "#id_wasp": "id_wasp",
        "#startDate": "datetime"
    },
    ExpressionAttributeValues: {
        ":id_wasp": id_wasp,
        ":datetime1": startDate,
        ":datetime2": endDate
    }
  };
  docClient.query(params, function(err, data) {
    if (err) {
      console.log(err.message)
      res.send({
        success: false,
        message: 'Error: Server error'
      });
    } else {
      outFilename = id_wasp + new Date().getTime().toString() + '.csv'
      utils.convert2CSV(data,outFilename, function(error, mes){
        if (error){
          res.send({
            success: false,
            message: error
          });
        }else{
          res.download(mes, outFilename, function(err) {
            if( err ) {
                console.log(err);
            }
        });
        }
        
      });
    }
  });
});